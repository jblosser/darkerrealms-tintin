# Darker Realms TinTin++ Configuration

This is a set of configuration files for tintin++ that works with Darker Realms LPMud (darkerrealms.org).

NOTE: You should make sure you're using at least version 2.02.41 of tintin++. Older versions definitely have issues with this configuration.

## Usage
If this is your only tintin++ configuration, you can get away with checking out this repo as `~/.tintin` and then running `tt++ ~/.tintin/rc.tin` from your home directory.

If you want or need to combine it with other configuration, look at `rc.tin`. You can edit this file to set `$DRTT_DIR` to the location you cloned this repo to and then `#read rc.tin` in your main configuration, or put similar lines in your main local configuration file and `#read` the files you want to use. Note: If you are using webtin, this is the approach you'll need to take.

Files in `$DRTT_DIR/dr/local/` should be edited as needed. `$DRTT_DIR/dr/local/session.tin` is read right before the connection to DR is made and should contain anything you need to do locally to override the default connection to `darkerrealms.org 2000`. `$DRTT_DIR/dr/local/local.tin` is run after all other configuration has been read and after the connection to the MUD has been made. Use it to read all your local configuration files, aliases, etc.

`$DRTT_DIR/tt-dr` and `$DRTT_DIR/dr/local/guild.tin` are provided as examples and are not read by anything by default.

## Features
This configuration inclues:

### Screen Splitting with Map Window

This configuration will automatically use a screen split of 2 rows on the bottom for the status bar, 2 more rows below that for the input bar, and the rest for the scroll window. To display a map, the following are provided:
* `/screen narrow` to split the top 25 rows of the screen for a map, appropriate for a normal full height 80 column window.
* `/screen wide` to split the left 80 columns for the scroll buffer and status bar and the rest for the map, appropriate for a full screen window.
* `/screen mobile` to split only top 12 rows for the map, appropriate for a phone.
* `/screen nomap` for the default split.

### Aliases

I didn't include most of mine because everyone has their own, but I included enough to get an idea how they work and the kinds of things they can do. Refer to the tintin++ docs for more information.

#### Basic Aliases
* `g [x]`: `get all [x]`
* `x <x>`: `exa <x>` (this is the same as `look at <x>`)
* `z <x>`: `search <x>`
#### Killing Things
* `k <target>`: `k` with an argument will add that target to tab completion, add the target as the current kill target, give a corpse to the target if you have one, then send `kill <target>` to the mud.
* `k`: `k` without a target will attempt to kill the current kill target, ie the last thing you attacked with `k <target>`.
#### Inventory Management
* `c [n]`: `get all from corpse [n]`
* `rc`: `drop corpse; get all from corpse`
#### Pub Aliases
* `bf`: Heal at main pub (`buy fire`).
* `bs`: Heal at TIka's and other `buy shot` pubs.
* `ndrink <drink> <sober1> <sober2> <sober3>`: Set up `bb` as an alias to heal with the given drink and foods. For pubs with a full selection of sober foods, use `ndrink <drink> <major sober> <medium sober>`. For pubs with only minor sober food (coffee, advil, etc.) use `ndrink <drink> <minor sober> <minor sober> <minor sober>`.
#### Movement Aliases
* `gcross|mcross|scross [container]`: Use a cross. If a container is given, get/put the cross from there. If not, try to use the on-game `%c` variable (set this on game with eg `var c duster`).
* `totrav`: Walk from Arnisdale church to the Traveler/Revenant area pub.
* `bktrav`: Reverse of the above.
#### Other Sample Aliases
* `enc <item>`: If you're a mage, cast enchant item. If not, try to buy a scroll and use it (only works in retail/magic shops).
* `perm <item>`: If you're a mage, cast permanency. If not, try to buy a scroll and use it (only works in retail/magic shops).
* `fin <target>`: `finger <target>`

### Highlight Colors

I have a pretty robust setup for automatically noting people's guilds by their title in `who` or room output and coloring their name appropriately when it shows up later. There's also a command to do this manually (`/watch_<guild> <Name>`) and another to add the highlight as well as add the name to tab completion and set up a tell alias (`/add_<guild> <Name> <alias>`, eg `/add_paladin Kestrel kes`). The colors are all similar to what's configured on Discord, but you can change them as well.

### Detailed Status Lines

To get the status line working, login and run `/prompt` once to set your character's game prompt to what it's looking for, and `toggle iacga on`.

After that it should parse the prompt, your score, and various other outputs to build a status bar that includes player name, HP/SP, wielded weapon, alignment, gold coins, percent of the way to next eternal level, current combat target's shape, wimpy settings, current reply target, and the time.

I also have good status bar configurations for most guilds. They aren't included in this repo but I can consider sharing them with individual players on request.

See the screenshots below for some samples.

### Mapping Shortcuts

DR doesn't support automapping protocols and the layout of many areas makes scripted automapping not make much sense. Instead I've written a collection of aliases as shortcuts for the built in mapper that I use to map manually as I explore. You can read about these below, as well as running `/map help` inside of tintin++. I've done thousands of rooms this way including some of the hairiest mazes on the game and it's quite fast once you get used to it. Note: `/map` commands are the ones I've written. The definitions can be viewed in this repo, or via `#alias`. Built-in tintin++ commands are found under `#map` and `#help map`.

I've included a basic map of the main town areas that are on the in-game maps, which should auto load when you start it up, and which you can add to.

The basics of mapping more with these aliases (most of these have short forms as well, like `/md`; see `/map help`):

* Make sure you are on the map in the correct room. It should start you in the login room on connection, then track you going through the game entrances. It will also track use of cross teleports. If you end up off the map anyway, use `/map goto church` to tell the map you're in mainland church. The other entrances are all defined `/map goto` targets as well: newland, arnisdale, academy, tutorial. You can add more using landmarks; see `/map landmark` below.
* When you walk into a room you haven't mapped, run `/map exits <exits>`, eg `/map exits n s e w`. This will mark the room as having those exits and set up the exits and the rooms on the other side, colored dark grey (bold black) to signal you haven't walked through them yet. Once you walk through an exit to the next room, run the `/map exits` command with the list of that room's exits. The room color will update to white to signal you have been in it, as will the exit you just walked through and any other exits connected to rooms you've been in.
  * tintin++ natively understands the exits n, s, e, w, u, d. Make sure to use those names instead of "north", "up", etc. It doesn't understand "in", "out", or anything more complex (eg "enter shack"), so you'll need to add those with more steps; see `/map dig` below and the native `#map dig` command.
  * If you don't see the dark grey exits and rooms, check your terminal isn't using dark grey as the background color. I use proper black for background; obviously you change the color being used, but it's written to the map, so you'll want to catch it early.
* If rooms start overlapping, you have two options:
  * `/map insert <direction>` will insert a spacer room in the given direction. Note you need to have already created the room on the other side.
  * `/map hide <direction>` will mark what's on the other side of the (already created) exit in the given direction as "hidden", meaning it won't draw on the map until you walk through the exit. You can mark one or both sides of an exit hidden, which allows for having the next area show up connected to the main map as you walk that way, or have a full portal-style "transition" from one map segment to another.
  * Whichever option you use in a given situation, it is very likely you'll need to delete some generated rooms (`#map delete`) and link things together manually (`/map link` for basic ones, `#map link` for complicated ones) to make it all line up. You will need room numbers to do this, which the next two commands display.
* `/map info` will show the map's internal info for the current room.
* `/map viewnums` to switch display of the room numeric codes on and off.
* `/map link <direction> to <room number>` to manually set an exit target.
* `/map goto <target>` will move the map's view of where you are to the given landmark or room number. If you eg go to main church, and the map doesn't track it for some reason, use `/mg church`.
* `/map move <direction>` will move you on the map only (not on the game) in the given direction. This is useful if you accidentally try to walk through a closed door and the map moves you anyway.
* `/map undo` to just undo the last map movement or add command.
* `/map x <direction>` is to be used when you try to walk through an exit and find it doesn't work (the area isn't open, etc.). It will walk you back the direction given and mark that room with an X.
* `/map dig <direction> as <map direction>` will create the room on the other side of a non-standard exit, like 'enter hut', but draw it on the map as the second argument, eg 'w'.
* `/map command <direction> via <command>` will set the command to be sent when you go through that exit, eg 'do unlock door; open door; e'.
* `/map path <landmark>` will determine the path to the given landmark, color it on the map, and print the direction string. Use this to help with creating static walk alises.
* `/map run <landmark>` will auto walk you to that room, while setting a temporary mark for the room you left, which `/map return` will then return you to. eg: `/map run tika` from anywhere that can path to Tika's, heal, then `/map return` to go back to the room you left. The `/map return` target will update when `/map return` is itself used, so after the previous two commands, `/map return` will alternate between going back to Tika's and returning to your point of origin.
  * For ease of typing on mobile, `mr` also work, for either command (they are technically interchangeable).
* `/map landmark <name>` will set a landmark for the current room which you can use in `/map run` or `/map goto`.
* `/map symbol <text>` will set the text to be shown on the map for that room.
* `/map note <text>` will write the given note to the current room.
* `/map query <text>` will show a list of landmarks and notes matching `<text`>.
* `/map write` will save to the default map location. Backups are up to you.

Again, these are all just shortcuts I made to help with using the mapping system with DR. You will still need the native tintin++ commands as well to get good maps. Hopefully reading the defintion of these aliases helps speed up learning the underlying commands. Some that I use a lot, which you should learn how to use, include:
* `#map delete`
* `#map dig`
* `#map exit`
* `#map exitflag`
* `#map landmark`
* `#map link`

### Logging

If you want fully automated logging of every session, all you need to do is set `$LOGFILE` to a writeable file path before the session is created.  `$DRTT/dr/local/session.tin` is an ideal place to do this.  With this set, logging will automatically start on session creation.  You can also set `$LOGTIMESTAMP` if you'd like the log lines timestamped via the builtin `#log timestamp`.

There are also some commands defined to manually interact with logging:

* `/log on` will manually turn logging on as long as `$LOGFILE` is set.
* `/log off` will manually turn logging off.
* `/log` with no argument will show current logging status (this is an alias for `#log info`).
* `/logline <text>` will send the given line to the currently active log if logging is on.  Use this in e.g. status line `#action` commands for lines that are being captured and then gagged if you want to log what was received.

## Screenshots

Widescreen layout with Cybertech status bar:
![Widescreen Layout w/Cybertech Status Bar](screenshot/wide.png)

Mobile layout:
![Mobile Layout](screenshot/mobile.png)

Narrow layout with guild color names and Barbarian status bar:
![Narrow layout w/Color Names and Barbarian Status Bar](screenshot/narrow.png)

Mage status bar (shows which spells are active):
![Mage Status Bar](screenshot/mage_status.png)
