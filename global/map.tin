#nop
/* this is the /map package */
#alias {/map help} {
   #sho {/map};
   #sho {};
   #sho {Note: /map commands are convenience aliases defined in darkerrealms-tintin. Definitions can be viewed via #alias.};
   #sho {Built-in tintin++ commands are found under #map and #help map.};
   #sho {};
   #sho {Exit handling:};
   #sho {};
   #sho {Primary:};
   #sho {/map exits <dir1> <dir2> <dir3...>: Note the exits of the current room on the map. Run this the first time you enter any room. (aka: /me)};
   #sho {/map hide <dir>: Mark rooms past the exit in the given direction as hidden, so they won't show up until you walk through the exit. Do this with both sides of an exit to create a "sub" map/area. (aka: /mh)};
   #sho {/map insert <dir>: Add a spacer room between this room and the next in the given direction. (aka: /mi)};
   #sho {/map link <exit> to <location>: Create an exit connecting to the given existing room number or landmark. (aka: /ml)};
   #sho {/map x <dir>: Mark the *current* room as inaccessible, and walk back the given direction on the map only. (aka: /mx)};
   #sho {};
   #sho {Secondary:};
   #sho {/map dig <exit> as <dir>: Create a new room reached via an exit that uses the given <exit> string ('climb tree', 'enter hut', etc.) but show it on the map connected in the given direction (u, w, etc.). Without this, non-standard directions will work but will not show connected on the map. (aka: /md)};
   #sho {/map link <exit> to <location> as <dir>: Create an exit using the given <exit> string to reach the given existing room number or landmark, but show it on the map conected in the given direction. (aka: /ml)};
   #sho {/map command <dir> via <command>: Set the MUD commands needed to go the given direction, eg 'do open door;e'. (aka: /mc)};
   #sho {/map delete <dir>: Delete the room in the given direction and the exit to it. (aka: /mdelete)};
   #sho {};
   #sho {Map movement (moves you on the map only):};
   #sho {/map goto <location>: Move on the map to the given landmark or room number. (aka: /mg)};
   #sho {/map move <dir>: Move on the map the given direction. (aka: /mm, mo)};
   #sho {/map undo: Undoes the last movement or room addition on the map. (aka: /mu)};
   #sho {};
   #sho {Landmark use and speedwalking:};
   #sho {/map symbol <text>: Set the text displayed on the map for this room. (aka: /ms)};
   #sho {/map landmark <name>: Define a landmark <name> in the current room. (aka: /mk)};
   #sho {/map landmarks [text]: List all landmarks, or all landmarks matching [text]. (aka: /mks)};
   #sho {/map path <location>: Find the given landmark or room number, mark the path on the map, and display a list of directions to walk there. (aka: /mp)};
   #sho {/map run <location>: Auto walk to the given landmark or room number. (aka: /mr, mr)};
   #sho {/map return: Auto walk to the last place you ran /map run or /map return. (aka: /mr, mr};
   #sho {};
   #sho {Quest note taking:};
   #sho {/map note <note>: Add a note to the current room. (aka: /mn)};
   #sho {/map notes <text>: Show all notes matching <text>. (aka: /mns)};
   #sho {/map query <text>: Show all rooms with landmarks or notes matching <text>. (aka: /mq)};
   #sho {};
   #sho {Room info:};
   #sho {/map info: Show the internal data for the current room. (aka: /mf)};
   #sho {/map viewnums: Toggle between showing the room numbers. (aka: /mv)};
   #sho {};
   #sho {Map saving:};
   #sho {/map read <path>: Read in a map and set the $MAPFILE, relative to <pwd>/dr/map/. (aka: /mread)};
   #sho {/map write: Save the map to the current $MAPFILE. (aka: /mw)};
}

#alias {/map color} {#map set {roomcolor} {%0}}
#alias {/mo} {/map color %0}

#alias {/map command %1 via %2} {#map exit %1 command {do %2}}
#alias {/mc %1 via %2} {/map command %1 via %2}

#alias {/map dig %1 as %2} {#map dig {%1}; #map exit {%1} dir {%2}}
#alias {/md %1 as %2} {/map dig %1 as %2}

#alias {/map exits} {
   #list EXITS create %0;
   #loop {1} {&EXITS[]} {i} {
      #var {EXIT} {$EXITS[$i]};
      #map get {roomexit} {EXIT_PRE};
      #if {&EXIT_PRE[$EXIT] == 0} {
         #map dig {$EXIT};
         #map get {roomexit} {EXIT_POST};
         #map set {roomcolor} {<008>} {$EXIT_POST[$EXIT]};
         #map exit {$EXIT} {color} {<108>} both;
      };
      #else {
         #map exit {$EXIT} {color} {<098>} both;
      };
      #map set {roomcolor} {<098>}
   }
}
#alias {/me} {/map exits %0}

#alias {/mec} {#map exit {%1} {color} {%2} both}

#alias {/map info} {#map info}
#alias {/mf} {/map info}

#alias {/map goto} {#map goto %0}
#alias {/mg} {/map goto %0}

#alias {/map hide} {
   #var {EXIT} %0;
   #map get {roomexit} {EXIT_PRE};
   #if {&EXIT_PRE[$EXIT] == 0} {
      #map dig {$EXIT};
      #map get {roomexit} {EXIT_POST};
      #map set {roomcolor} {<008>} {$EXIT_POST[$EXIT]};
      #map exitflag {$EXIT} hide on;
      #map exit {$EXIT} {color} {<169>} both;
   };
   #else {
      #map exitflag {$EXIT} hide on;
      #map exit {$EXIT} {color} {<169>} both;
   };
   #map set {roomcolor} {<098>}
}
#alias {/mh} {/map hide %0}

#alias {/map insert} {#map insert %0 void}
#alias {/mi} {/map insert %0}

#alias {/map landmark} {#map get {roomvnum} {VNUM}; #map landmark {%0} {$VNUM}}
#alias {/mk} {/map landmark %0}

#alias {/map landmarks} {#map landmark}
#alias {/mks} {/map landmarks}

#alias {/map landmarks %1} {#map landmark {{.*%1.*}}} {4}
#alias {/mks %1} {/map landmarks %1} {4}

#alias {/map link %1 to %2} {#map link {%1} {%2}}
#alias {/ml %1 to %2} {/map link %1 to %2}

#alias {/map link %1 to %2 as %3} {#map link {%1} {%2}; #map exit {%1} dir {%3}} {4}
#alias {/ml %1 to %2 as %3} {/map link %1 to %2 at %3} {4}

#alias {/map move} {#map move %0}
#alias {/mm} {/map move %0}

#alias {/map note} {#map set {roomnote} {%0}}
#alias {/mn} {/map note %0}

#alias {/map notes} {
   #map list {roomnote}{{.*%0.*}} {variable} {NOTES};
   #foreach {*NOTES[%*]} {NOTEVNUM} {
      #map get {roomnote} {NOTETEXT} {$NOTEVNUM};
      #format {nline} {%s: %s} {$NOTEVNUM} {$NOTETEXT};
      #sho {$nline};
   };
}
#alias {/mns} {/map notes %0}

#alias {/map new} {#var MAPFILE maps/%0; #sho {Creating new map at $MAPFILE...}; #map create; #map goto 1; #map flag vtmap; #map flag static}

#alias {/map path} {#map find %0; #path map}
#alias {/mp} {/map path %0}

#alias {/map query} {
   #sho {Landmarks:};
   /map landmarks %0;
   #sho {Notes:};
   /map notes %0
}
#alias {/mq} {/map query %0}

#alias {/map return} {#map get {roomvnum} {TMP};#map run {$PREV_ROOM};#var {PREV_ROOM} {$TMP}}
#alias {/mr} {/map return}
#alias {/map run} {/map return}
#alias {/map run %1} {#map get {roomvnum} {PREV_ROOM};#map run {%1}} {4}
#alias {/mr %1} {/map run %1} {4}

#alias {/map symbol} {#map set {roomsymbol} {%0}}
#alias {/ms} {/map symbol %0}

#alias {/map undo} {#map undo}
#alias {/mu} {/map undo}

#alias {/map viewnums} {#map flag asciivnums}
#alias {/mv} {/map viewnums}

#alias {/map write} {#sho {Saving to $MAPFILE...}; #map write $MAPFILE}
#alias {/mwrite} {/map write}
#alias {/msave} {/map write}
#alias {/mw} {/map write}

#alias {/map x} {#map set {roomcolor} {<109>}; #map exit {%0} {color} {<079>} both; #map set {roomsymbol} {X}; #map goto {%0}}
#alias {/mx} {/map x %0}

#alias {mo} {/mm}
#alias {mr} {/mr}

#map create
#map goto 1
#map flag vtmap
#map flag static
